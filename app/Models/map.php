<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $fillable = ['name', 'country', 'city', 'x', 'y', 'comment'];

    public static function getValidationRules()
    {
        // проинициализируем массив статичеческими правилами
        $validation = array(
            'name' => 'required',
            'country' => 'required',
            'city' => 'required',
            'x' => 'required|integer',
            'y' => 'required|integer',
            'comment' => 'required',
        );
        return $validation;
    }

    public static function search($input)
    {
        $map = Map::where('id', '>', 0);
        if (!empty($input ['name'])) {
            $map = $map->where('name', '=', $input['name']);
        }
        if (!empty($input ['city'])) {
            $map = $map->where('city', '=', $input['city']);
        }
        if (!empty($input ['country'])) {
            $map = $map->where('country', '=', $input['country']);
        }
        if (!empty($input ['x'])) {
            $map = $map->where('x', '=', $input['x']);
        }
        if (!empty($input ['y'])) {
            $map = $map->where('y', '=', $input['y']);
        }
        $map = $map->get();
        return $map;
    }

    public static function takeall()
    {
        $map = Map::all();
        return $map;
    }
}