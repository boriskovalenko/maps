<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\map;
use Validator, Redirect, Request;

class IndexController extends Controller {
    public function getIndex() {
        $maps = Map::orderBy('created_at', 'DESC')->take(6)->get();
        $counter = Map::count();
        return View('index', array(
            'maps'  => $maps,
            'counter'  => $counter,
        ));
    }
}