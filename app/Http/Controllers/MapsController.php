<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\map;
use App\Models\country;
use App\Models\city;

use Validator, Redirect, Request, Compiled;

class MapsController extends Controller
{
    public function getAdd()
    {
        $country = Country::takeall()->lists('name', 'name');
        return View('/maps/addmap', ['country' => $country]);
    }

    public function postAdd()
    {
        $input = Request::all();
        $validation = Validator::make($input, Map::getValidationRules());
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }
        if ($validation->passes()) {
            // дальннейшая логика с данными
            $map = Map::create($input);
            return View('/maps/view', array('map' => $map));
        }
    }

    public function getView($mapId)
    {
        $map = Map::find($mapId);

        // Если такой нет, то вернем пользователю ошибку 404 - Не найдено
        if (!$map) {
            echo('not found' . $mapId);
            exit;
        }

        return View('/maps/view', array('map' => $map));
    }

    public function getSrch()
    {
        $country = Country::takeall()->lists('name', 'name');
        foreach ($country as $key => $value) {
            $arr[] = $value;
        }
        $jscountry = json_encode($arr);
        return View('/maps/srch', ['country' => $country, 'jscountry' => $jscountry]);
    }

    public function postSrch()
    {
        $input = Request::all();
        $map = Map::search($input);

        return View('/maps/searchview', ['map' => $map]);
    }

    public function getAll()
    {
        $allmaps = Map::takeAll();

        return View('/maps/searchview', ['map' => $allmaps]);
    }
}
