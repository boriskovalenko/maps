<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->index();
			$table->string('country')->index();
			$table->string('city')->index();
			$table->integer('x');
			$table->integer('y');
			$table->string('user');
			$table->integer('user_id')->index()->unsigned();
			$table->text('comment');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maps');
	}

}
