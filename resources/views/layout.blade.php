<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">

    @yield('script')

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">(BORIS)</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav nav-pills navbar-nav">
                    <li><a href="{{ url('/') }}">На главную</a></li>
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            Достопримечательности <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right navbar-inverse" role="menu">
                            <li><a href="/map/all">Все</a></li>
                            <li><a href="/map/srch">Найти</a></li>
                            <li><a href="/map/add">Добавить</a></li>
                            <li><a href="//localhost/maps/acompl.html">Acompl</a></li>
                        </ul>
                    </li>
                    <li><a href=#>Мои достопримечательности</a></li>
                    <li><a href=#>Карта</a></li>
                    <li><a href=#>Контакты</a></li>
                </ul>

                <ul class="nav nav-pills navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Войти</a></li>
                        <li><a href="{{ url('/auth/register') }}">Регистрация</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Выйти</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid50">
        <div class="row">
            <div class="col-md-2">
                <nav class="navbar-default">
                        <ul class="nav navbar-sidebar navbar-inverse">
                            <li role="presentation" class="dropdown navbar-default">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                    Достопримечательности <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu navbar-inverse" role="menu">
                                    <li><a href="/map/all">Все</a></li>
                                    <li><a href="/map/srch">Найти</a></li>
                                    <li><a href="/map/add">Добавить</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Мои достопримечательности</a></li>
                            <li><a href=#>Карта</a></li>
                            <li><a href=#>Контакты</a></li>
                        </ul>
                </nav>
            </div>
            <div class="col-md-10">
                @yield('content')
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container-fluid navbar-fixed-bottom">
            <div class="col-md-12">
                <div class="navbar-right">
                    2015 Maps &copy;
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
