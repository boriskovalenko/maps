@extends('layout')

@section('title')
    База туристических достопримечательностей
@endsection

@section('content')
    <div class="container-fluid">
        <br>
        <div class="jumbotron">
            <h1>Welcome!</h1>
            <h2>This is a resource for travellers. Created to share the information about interesting places all around the world.</h2>
            <h2>Enjoy it :)</h2>
            <br><hr><br>
            <tr><a class="btn btn-primary" href="/map/all" role="button"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Все <br> достопримечательности</a>
                <a class="btn btn-primary" href="/map/srch" role="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Найти <br> достопримечательность</a>
                <a class="btn btn-primary" href="/map/add" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить <br> достопримечательность</a>
                <a class="btn btn-primary" href="#" role="button"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Карта <br> достопримечательностей</a>
            </tr>
        </div>
    </div>
@endsection