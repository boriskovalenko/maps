@extends('layout')
@section('title')
    Добавление достоприрмечательности
@endsection
@section('content')
    <div>
        <div class="container">
             @if ($errors->all())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <h2>Добавление достопримечательности</h2>
            {!! Form::open(array('url' => action('MapsController@postAdd'),
            'method' => 'post',
            'role' => 'form',
            'class' => 'form-horizontal')) !!}
            @include('maps/form', ['country' => $country])
            <div class="form-group">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-primary submit-button">Добавить</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection