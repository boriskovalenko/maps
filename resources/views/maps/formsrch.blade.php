<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Имя</label>
    <div class="col-sm-5">
        {!! Form::text('name', null, array('class' => 'form-control')) !!}
    </div>
</div>
<div class="form-group">
    <label for="country" class="col-sm-2 control-label">Страна</label>
    <div class="col-sm-5">
        {!! Form::select('country', $country, 0, array('class' => 'form-control')) !!}
    </div>
</div>
<div class="form-group">
    <label for="city" class="col-sm-2 control-label">Город</label>
    <div class="col-sm-5">
        {!! Form::text('city', null, array('class' => 'form-control', 'id' => 'autocomplete')) !!}
    </div>
</div>
<div class="form-group">
    <label for="coordinates" class="col-sm-2 control-label">Координаты точки:</label>
</div>
<div class="form-group">
    <label for="x" class="col-sm-2 control-label">X</label>
    <div class="col-sm-5">
        {!! Form::text('x', null, array('class' => 'form-control')) !!}
    </div>
</div>
<div class="form-group">
    <label for="y" class="col-sm-2 control-label">Y</label>
    <div class="col-sm-5">
        {!! Form::text('y', null, array('class' => 'form-control')) !!}
    </div>
</div>