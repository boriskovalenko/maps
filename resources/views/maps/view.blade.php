@extends('layout')
@section('title')
    Достопримечательность
@endsection
@section('content')
    <div class="container-fluid">
        <h2 align="center">Достопримечательность</h2>
        <br>
        <table class="table table-hover table-bordered">
            <tr>
                <td> Имя </td>
                <td> {{{ $map['name'] }}} </td>
            </tr>
            <tr>
                <td> Страна </td>
                <td> {{{ $map['country'] }}} </td>
            </tr>
            <tr>
                <td> Город </td>
                <td> {{{ $map['city'] }}} </td>
            </tr>
            <tr>
                <td> Координаты точки </td>
                <td> X: {{{ $map['x'] }}} <br>
                     Y: {{{ $map['y'] }}} </td>
            </tr>
            <tr>
                <td> Комментарий </td>
                <td> {{{ $map['comment'] }}} </td>
            </tr>
        </table>
        <a href='/' class="btn btn-primary" role="button">На главную</a>
    </div>
@endsection