@extends('layout')
@section('title')
    Поиск достоприрмечательности
@endsection
@section('script')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function () {
            var availableTags = JSON.parse('<?php echo ($jscountry);?>');
            $("#autocomplete").autocomplete({
                source: availableTags
            });
        });
    </script>
@endsection
@section('content')
    <div>
        <div class="container col-md-12">
            @if ($errors->all())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <h2>Поиск достопримечательности</h2>
            <br>

            <p> Заполните необходимые поля для определения критериев поиска</p>
            <br>
            {!! Form::open(array('url' => action('MapsController@postSrch'),
            'method' => 'post',
            'role' => 'form',
            'class' => 'form-horizontal')) !!}
            @include('maps/formsrch', ['country' => $country])
            <div class="form-group">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-5">
                    <button type="submit" class="btn btn-primary submit-button">Найти</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection