@extends('layout')
@section('title')
    Достопримечательность
@endsection
@section('content')
    <div class="container-fluid">
        <h2 align="center">Достопримечательность</h2>
        <br>
        <table class="table table-hover table-bordered">
            <tr>
                <?php
                    if (!empty($map)) {
                        echo "<tr>";
                        echo "<td><b> № </b></td>";
                        echo "<td><b> Название </b></td>";
                        echo "<td><b> Страна </b></td>";
                        echo "<td><b> Город </b></td>";
                        echo "<td><b> Координата X </b></td>";
                        echo "<td><b> Координата Y </b></td>";
                        echo "<td><b> Комментарий </b></td>";
                        echo "</h2></tr>";
                        foreach ($map as $maps) {
                            echo "<tr>";
                            echo "<td> $maps->id </td>";
                            echo "<td> $maps->name </td>";
                            echo "<td> $maps->country </td>";
                            echo "<td> $maps->city </td>";
                            echo "<td> $maps->x </td>";
                            echo "<td> $maps->y </td>";
                            echo "<td> $maps->comment </td>";
                            echo "</tr>";
                        }
                    }
                    else echo 'Не найдено подходящих достопримечательностей';
                ?>
            </tr>
        </table>
        <a href='/' class="btn btn-primary" role="button">На главную</a>
    </div>
@endsection